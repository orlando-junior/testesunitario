package br.ce.orlando.suites;

//import org.junit.runner.RunWith;
//import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

//import br.ce.orlando.servicos.CalculadoraTest;
import br.ce.orlando.servicos.CalculoValorLocacaoTest;
import br.ce.orlando.servicos.LocacaoServiceTest;

//@RunWith(Suite.class)
@SuiteClasses({
//	CalculadoraTest.class,
	CalculoValorLocacaoTest.class,
	LocacaoServiceTest.class
	
})
public class SuiteExecucao {
	
}

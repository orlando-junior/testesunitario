package br.ce.orlando.servicos;

import br.ce.orlando.entidades.Usuario;

public interface EmailService {
	
	public void notificarAtraso(Usuario usuario);
}

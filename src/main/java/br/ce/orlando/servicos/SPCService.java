package br.ce.orlando.servicos;

import br.ce.orlando.entidades.Usuario;

public interface SPCService {
	
	public boolean possuiNegativacao(Usuario usuario) throws Exception;
}

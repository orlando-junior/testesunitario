package br.ce.orlando.daos;

import java.util.List;

import br.ce.orlando.entidades.Locacao;

public interface LocacaoDAO {
	
	public void salvar(Locacao locacao);

	public List<Locacao> obterLocacoesPendentes();
}

package br.ce.orlando.exceptions;

public class LocadoraException extends Exception{

	private static final long serialVersionUID = 6051381502562225262L;

	public LocadoraException(String message) {
		super(message);
	}
	
}
